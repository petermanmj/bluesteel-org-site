---
title: "Learning Powershell"
date: 2020-07-31T17:11:31-07:00
draft: false
hidden: false
---

For the record, I've never liked Cygwin. Even when work dictated use of Windows, I used [UnxUtils](http://unxutils.sourceforge.net/) which work natively on Windows rather than through the Cygwin DLL. I'm sure Cygwin is fine - it just wasn't for me. 

Fast forward to today - I generally use a Mac laptop for work but for home / project use it's always been linux. The WSL in Windows 10 means I can reliably use Windows 10 as my linux machine - I _never_ thought I'd say that but it's just that good. That said it's been nagging at me that Powershell is on my Windows machine and I really don't know how to use it ... so I decided to fix that.

It was easier than expected to switch from typing `ls` to `gci` but what is `gci`? Turns out the Powershell commands are founded in some common sense. `gci` is an alias for Get-ChildItem - which follows the pattern `verb-noun`. Turns out there is even a good command discovery, er, command: `Get-Command` or `gc`. A rough translation table to get started is something like this:

| sh | pwsh |Long Name|
|----|------|---------|
|ls | gci|Get-ChildItem|
|cd | sl|Set-Location|
|touch | ni|New-Item|
|pwd | gl|Get-Location|
|nslookup|n/a|Resolve-DnsName|

That should get you started. I threw the last one in there as a wildcard. Really learning the Powershell model is what will enable productivity. See, everything returned is an object. You may be used to running `ls -lt` to get a (long) list of items sorted by modification time but in Powershell that would be `gci | Sort-Object LastWriteTime` since you want to sort the output of the "call" to `gci`. Functions can be called on command results: `(gi .).getFiles()` or `(gi .).getDirectories()` which feels like a lot of typing. The saving grace is very good tab completion for almost everything you type.

I've only scratched the surface of what Powershell can do but it's really quite good. I know it's easy to poke fun at a Microsoft cli - cmd could only go so far - but this is really well thought out and pretty fun to use. Need to know about a command, just add `-?` and it'll tell you.
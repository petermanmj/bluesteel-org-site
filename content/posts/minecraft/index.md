---
title: "Minecraft"
date: 2020-04-18T17:11:31-07:00
draft: false
hidden: true
---

Stuck at home? We all are, so why not play some Minecraft together!

Simple directions to connect to the 199 Quarantine Minecraft Server, you were provided the hostname and port so use them where directed:

## Step 1
![image](mc1.png)

## Step 2
![image](mc2.png)

## Step 3
![Three](mc3.png)

## Step 4
![Four](mc4.png)
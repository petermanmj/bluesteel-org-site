---
title: "About"
description: "Michael Peterman - Family Guy, Software Tinkerer, Maker of things"
featured_image: '/images/hero.png'
---
I'm always on the lookout for ways to make better things and have fun in life. I'm the guy who brings bike chain oil to the grocery store to silence that squeaky cart wheel - or, as my wife calls it: date night.

Professionally, I've worked in digital media rights management and delivery, mobile services, high-throughput business-to-consumer messaging (think 1MM+ per second) and consumer identity. I currently lead a team of architects building the best data platforms around. I'm passionate about strong security and doing right by the consumer. I hold a [CISSP](https://www.isc2.org/Certifications/CISSP) certification to back that up. I've even been known to talk in public: https://youtu.be/_lWqRn_Sb74 (it's been a while though)

When not building software systems I'm with my family and friends enjoying my home roasted coffe or home brewed beer between cycling and skiing outings.

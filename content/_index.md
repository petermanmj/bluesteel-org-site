---
title: "Michael Peterman"
featured_image: '/images/hero.png'
description: "Family Guy, Software Development Leader, Dog Herder, Tinkerer, Brewer of Beer, Roaster of Coffee"
---

This is my personal website for various ramblings and musings - ideas and content are my own.
